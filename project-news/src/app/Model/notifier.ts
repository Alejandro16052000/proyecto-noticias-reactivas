import { news } from './news';

export class notifier{
    idnotifier: number;
    notifiername: string;
    notifiersurname: string;
    notifierpassword: string;
    notifiercountry: string;
    notifierage: number;
    news: news[];
}