import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { LoginComponent } from './components/login/login.component';
import { NewsComponent } from './components/news/news.component';
import { CnewsComponent } from './components/CRUD/cnews/cnews.component';
import { UnewsComponent } from './components/CRUD/unews/unews.component';
import { DnewsComponent } from './components/CRUD/dnews/dnews.component';
import { NewsServicesService } from './services/news-services.service';
import { NotifierServicesService } from './services/notifier-services.service';
import { FormsModule } from  '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RnewsComponent } from './components/CRUD/rnews/rnews.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    LoginComponent,
    NewsComponent,
    CnewsComponent,
    UnewsComponent,
    DnewsComponent,
    RnewsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [NewsServicesService, NotifierServicesService],
  bootstrap: [AppComponent]
})
export class AppModule { }
