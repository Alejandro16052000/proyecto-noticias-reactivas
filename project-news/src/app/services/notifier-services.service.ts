import { Injectable } from '@angular/core';
import {HttpClient } from '@angular/common/http';
import { notifier } from '../Model/notifier';

@Injectable({
  providedIn: 'root'
})
export class NotifierServicesService {

  constructor(private http:HttpClient) { }

  Url='http://localhost:8080/notifier';

  getNotifier(){
    return this.http.get<notifier[]>(this.Url);
  }

  createNotifier(notifier:notifier){
    return this.http.post<notifier>(this.Url,notifier);
  }

  getNotifierId(idnotifier:number){

    return this.http.get<notifier>(this.Url+"/"+idnotifier);

  }

  updateNotifier(notifier:notifier){

    return this.http.put<notifier>(this.Url+"/"+notifier.idnotifier,notifier);
  }

  deleteNotifier(notifier:notifier){
    return this.http.delete<notifier>(this.Url+"/"+notifier.idnotifier);
  }

}
