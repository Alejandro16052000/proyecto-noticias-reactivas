import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DnewsComponent } from './dnews.component';

describe('DnewsComponent', () => {
  let component: DnewsComponent;
  let fixture: ComponentFixture<DnewsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DnewsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DnewsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
