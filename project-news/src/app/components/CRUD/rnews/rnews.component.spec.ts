import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RnewsComponent } from './rnews.component';

describe('RnewsComponent', () => {
  let component: RnewsComponent;
  let fixture: ComponentFixture<RnewsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RnewsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RnewsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
