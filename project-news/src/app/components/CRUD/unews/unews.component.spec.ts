import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UnewsComponent } from './unews.component';

describe('UnewsComponent', () => {
  let component: UnewsComponent;
  let fixture: ComponentFixture<UnewsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UnewsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UnewsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
