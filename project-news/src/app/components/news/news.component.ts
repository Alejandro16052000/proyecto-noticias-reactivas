import { Component, OnInit } from '@angular/core';
import { news } from 'src/app/Model/news';
import { Router } from '@angular/router';
import { NewsServicesService } from 'src/app/services/news-services.service';

@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.css']
})
export class NewsComponent implements OnInit {
 
  private idnotifier: number;
  public newses: news[];
  private newsid: number;
  constructor(private router:Router, private service:NewsServicesService) { }

  ngOnInit(): void {
    
    this.service.getNews()
    .subscribe(data=>{
      this.newses = data;
    })
  }

  back(){
    this.router.navigate([""]);
  }
}
